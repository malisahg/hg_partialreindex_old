<?php

namespace Hg\Partialreindex\Block\Indexer\Backend;

class Container extends \Magento\Indexer\Block\Backend\Container {

    protected function _construct() {

        parent::_construct();

        $this->addButton(
            'partial_reindex', [
                'label' => "Partial Reindex",
                'onclick' => 'if (confirm(\'Partial Reindex can slow down the site. Are you sure you want to do it now?\')) { setLocation(\'' . $this->getPartialReindexUrl() . '\'); } ',
                'class' => ' primary'
            ]
        );
    }

    public function getPartialReindexUrl() {
        return $this->getUrl('partialreindex/partialreindex/run');
    }

}
