<?php

namespace Hg\Partialreindex\Console\Command;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;

use Magento\Framework\Indexer\ConfigInterface;
use Magento\Framework\Indexer\IndexerInterface;
use Magento\Framework\Indexer\IndexerInterfaceFactory;
use Magento\Framework\Indexer\StateInterface;


class PartialReindexCommand extends Command
{

    var $processor;
    
    public function __construct(
            \Magento\Indexer\Model\Processor $processor,
            $name = null
            ) {

        $this->processor = $processor;
        parent::__construct($name);
        
    }
 
    protected function configure()
    {
        $this->setName('hg:partial_reindex')->setDescription('Partial reindex.');
    }
 
    protected function execute(InputInterface $input, OutputInterface $output )
    {
        $output->writeln('Partial Reindex Start!');
        $this->processor->updateMview();
        $output->writeln('Partial Reindex Finish!');
    }
 

}