<?php

namespace Hg\Partialreindex\Controller\Adminhtml\Partialreindex;

class Run extends \Magento\Backend\App\Action {

    protected $_processor;

    public function __construct(
            \Magento\Backend\App\Action\Context $context, 
            \Magento\Indexer\Model\Processor $processor
        ) {

        $this->_processor = $processor;        
        parent::__construct($context);
    }

    public function execute() {

        try {
            $this->_processor->updateMview();
            $this->messageManager->addSuccess(__('Partial Reindex are complete successful.'));
        } catch (Exception $e) {
       
            $this->messageManager->addError(__('Partial Reindex Fail'));
        }
        $this->_redirect('indexer/indexer/list');
    }

}
